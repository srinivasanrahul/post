//
//  ChatViewController.swift
//  Post
//
//  Created by Ramesh Vayavuru on 04/12/16.
//  Copyright © 2016 Ramesh Vayavuru. All rights reserved.
//

import UIKit
import syncano_ios
import JSQMessagesViewController
import MobileCoreServices
import FirebaseStorage
import FirebaseDatabase
import FirebaseAuth

class ChatViewController: JSQMessagesViewController,  UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var ref = FIRDatabase.database().reference()
    let incomingBubble = JSQMessagesBubbleImageFactory().incomingMessagesBubbleImage(with: UIColor(red: 10/255, green: 180/255, blue: 230/255, alpha: 1.0))
    let outgoingBubble = JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImage(with: #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1))
    var messages = [JSQMessage]()
    let imagePicker = UIImagePickerController()
    var messageObject = Messages()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        self.inputToolbar.contentView.rightBarButtonItem.setTitle("Post", for: .normal)
        
        setup();
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let  channelRef = ref.child("Channels/test")
        self.messages.removeAll()
        
        channelRef.queryOrdered(byChild: "timeStamp").observeSingleEvent(of: .value, with: { snapshot in
            if let snapDict = snapshot.value as? [String: AnyObject]{
                  self.messages.removeAll()
                for each in snapDict{
                  print(each.key)
                  print(each.value["messageValue"] as! String)
                  let senderId = each.value["senderID"] as! String
                  let senderDisplayName = each.value["senderID"] as! String
                  let message = JSQMessage(senderId: senderId, displayName: senderDisplayName, text: each.value["messageValue"] as! String)
                  self.messages.append(message!)
                  self.finishSendingMessage()
                  
                }
                                

            }
        })
        
//        channelRef.queryOrdered(byChild: "timeStamp").observe(.value , with: { snapshot in
//            
//            if let snapDict = snapshot.value as? [String: AnyObject] {
//                self.messages.removeAll()
//                NSSortDescriptor(key: "timeStamp", ascending: true)
//                for each in snapDict{
//                    print(each.key)
//                    print(each.value["messageValue"] as! String)
//                    let senderId = each.value["senderID"] as! String
//                    let senderDisplayName = each.value["senderID"] as! String
//                    let message = JSQMessage(senderId: senderId, displayName: senderDisplayName, text: each.value["messageValue"] as! String)
//                    self.messages.append(message!)
//                    self.finishSendingMessage()
//                }
//                
//            }
//        })
       
        
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        
        let senderId = senderId
        let senderDisplayName = "abc"
        let message = JSQMessage(senderId: senderId, senderDisplayName: senderDisplayName, date: date, text: text)
        self.messages.append(message!)
        self.finishSendingMessage()
        storeMessageToFir(message: text, senderId: senderId!)
        
    }
    
    override func didPressAccessoryButton(_ sender: UIButton!) {
        
        self.inputToolbar.contentView!.textView!.resignFirstResponder()
        
        let sheet = UIAlertController(title: "Media messages", message: nil, preferredStyle: .actionSheet)
        
        let photoAction = UIAlertAction(title: "Send photo", style: .default) { (action) in
            self.takeImage()
        }
        
        let videoAction = UIAlertAction(title: "send video", style: .default){(action) in
        self.takeVideo()
        }
        
        let locationAction = UIAlertAction(title: "Send location", style: .default) { (action) in
            let locationItem = self.buildLocationItem()
            self.addMedia(locationItem)
        }
        
        let audioAction = UIAlertAction(title: "Send audio", style: .default) { (action) in
//           let audioItem = self.buildAudioItem()
//            self.addMedia(audioItem)
            
            self.recordAudioAction()
        }
        
        let sendFromCameraRoll = UIAlertAction(title: "send from Camera Roll", style: .default){(action) in
           
            self.showImagePickerForChooseExisting()

        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        sheet.addAction(photoAction)
        sheet.addAction(videoAction)
        sheet.addAction(audioAction)
        sheet.addAction(locationAction)
        sheet.addAction(sendFromCameraRoll)
        sheet.addAction(cancelAction)
        
        self.present(sheet, animated: true, completion: nil)
    }
    
    func showImagePickerForCaptureImageVideo(type: String) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            self.imagePicker.mediaTypes = [type]
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }else {
            print("You don't have camera")
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        let data = self.messages[indexPath.row]
        return data
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let data = messages[indexPath.row]
        switch(data.senderId) {
        case self.senderId:
            return self.outgoingBubble
        default:
            return self.incomingBubble
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellBottomLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        let data = messages[indexPath.row]
        switch(data.senderId) {
        case self.senderId:
            return NSAttributedString(string: "sent       \t")
        default:
            return NSAttributedString(string: "")
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAt indexPath: IndexPath!) -> CGFloat {
        
        return 10.0
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        let data = messages[indexPath.row]
        switch(data.senderId) {
        case self.senderId:
            
            return JSQMessagesAvatarImageFactory.avatarImage(withUserInitials: "SR", backgroundColor: #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), textColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), font:
                UIFont.systemFont(ofSize: 9), diameter: 20)
        default:
            return JSQMessagesAvatarImageFactory.avatarImage(withUserInitials: "SK", backgroundColor: #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1), textColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), font: UIFont.systemFont(ofSize: 9), diameter: 20)
        }
    }
    
    
    func showImagePickerForChooseExisting() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            self.imagePicker.mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String]
            self.imagePicker.allowsEditing = true
            
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    func takeImage(){
            showImagePickerForCaptureImageVideo(type: kUTTypeImage as String)
    }
    
    func takeVideo(){
           showImagePickerForCaptureImageVideo(type: kUTTypeMovie as String)
    }
    
    func recordAudioAction() {
        
//        let controller = IQAudioRecorderViewController()
//        controller.delegate = self
//        controller.title = "Record Audio"
//        controller.maximumRecordDuration = 50
//        controller.allowCropping = false
//        controller.barStyle = .default;
//        self.presentBlurredAudioRecorderViewControllerAnimated(controller)
        //        controller.normalTintColor = UIColor.magenta
        //        controller.highlightedTintColor = UIColor.orange
        //        controller.barStyle = UIBarStyleBlack;
        //        [self presentAudioRecorderViewControllerAnimated:controller];
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        if mediaType == kUTTypeImage {
            if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                var image = originalImage.resizeWith(percentage: 0.5)
                let photoItem = JSQPhotoMediaItem(image: image)
                image = image?.resizeWith(percentage: 0.5)
               uploadimage(image: image!)
                self.addMedia(photoItem!)
            } else{
                print("Something went wrong")
            }
        } else {
            if let videoURL = info["UIImagePickerControllerMediaURL"] as? NSURL {
                let videoItem = self.buildVideoItem(videoUrl: videoURL as URL)
                self.addMedia(videoItem)
            } else{
                print("Something went wrong")
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func buildVideoItem(videoUrl: URL) -> JSQVideoMediaItem {
        
        //        let sample = Bundle.main.path(forResource: "video", ofType: "MOV")
        //        let videoURL = URL(fileURLWithPath: sample!)
        let videoItem = JSQVideoMediaItem(fileURL: videoUrl, isReadyToPlay: true)
        
        return videoItem!
    }
    
    func buildAudioItem() -> JSQAudioMediaItem {
        let sample = Bundle.main.path(forResource: "jsq_messages_sample", ofType: "m4a")
        let audioData = try? Data(contentsOf: URL(fileURLWithPath: sample!))
        
        let audioItem = JSQAudioMediaItem(data: audioData)
        
        return audioItem
    }
    
    func buildLocationItem() -> JSQLocationMediaItem {
        let ferryBuildingInSF = CLLocation(latitude: 37.795313, longitude: -122.393757)
        
        let locationItem = JSQLocationMediaItem()
        locationItem.setLocation(ferryBuildingInSF) {
            self.collectionView!.reloadData()
        }
        
        return locationItem
    }
    
    func addMedia(_ media:JSQMediaItem) {
        let message = JSQMessage(senderId: self.senderId, displayName: self.senderDisplayName, media: media)
        self.messages.append(message!)
        
        //Optional: play sent sound
        
        self.finishSendingMessage(animated: true)
    }
    
    func reloadMessagesView() {
        self.collectionView?.reloadData()
    }
    
    func setup() {
        self.senderId = UIDevice.current.identifierForVendor?.uuidString
        self.senderDisplayName = UIDevice.current.identifierForVendor?.uuidString
    }
    
    func uploadimage(image: UIImage){
        let storageRef = FIRStorage.storage().reference()
        let uploadData = UIImagePNGRepresentation(image)

        storageRef.child("Images/\(UUID().uuidString).jpg").put(uploadData!, metadata: nil, completion:{ (metadata, error) in
            if error != nil{
            print(error)
            }else{
                print(metadata!.downloadURL)
            print("Uploaded image")
            }
            })
    }
    
    func storeMessageToFir(message: String, senderId: String) {
        let dictionary  = ["messageType":"TEXT", "messageValue":"\(message)", "senderID": senderId, "timeStamp": FIRServerValue.timestamp()] as [String : Any]
        ref.child("Channels").child("test").child(UUID().uuidString).setValue(dictionary)
   
    }
    
}


extension UIImage {
    func resizeWith(percentage: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: size.width * percentage, height: size.height * percentage)))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
    func resizeWith(width: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
}

