//
//  ChatTableViewCell.swift
//  Post
//
//  Created by Ramesh Vayavuru on 03/12/16.
//  Copyright © 2016 Ramesh Vayavuru. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell {

    
    @IBOutlet weak var ProfileDisplayName: UILabel!
    @IBOutlet weak var ProfileImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
