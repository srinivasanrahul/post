//
//  SignIn_and_SignUpViewController.swift
//  Post
//
//  Created by Ramesh Vayavuru on 19/11/16.
//  Copyright © 2016 Ramesh Vayavuru. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseInstanceID
import LocalAuthentication
import FirebaseDatabase
import FirebaseStorage
class SignIn_and_SignUpViewController: UIViewController {

    @IBOutlet weak var InfoLabel: UILabel!
    @IBOutlet weak var JoinButton: UIButton!
    @IBOutlet weak var SignInButton: UIButton!
    @IBOutlet weak var EmailTextField: UITextField!
    @IBOutlet weak var PasswordTextField: UITextField!
    @IBOutlet weak var TouchIDButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
    
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.hidesBackButton = true
        EmailTextField.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func JoinButtonTouchUpInsideAction(_sender: UIButton){
        FIRAuth.auth()?.createUser(withEmail: EmailTextField.text!, password: PasswordTextField.text!) { (user, error) in
            if (error != nil) {
                print(error)
            } else {
                if let user = user {
                    let uid = user.uid  // Unique ID, which you can use to identify the user on the client side
                    let email = user.email
                    let photoURL = user.photoURL
                    
                    var ref: FIRDatabaseReference!
                    
                    ref = FIRDatabase.database().reference()
                    
                        ref.child("users").child(user.uid).setValue(["username": user.email])
                   
                }
                let DashboardVC = Utils.getStoryBoard().instantiateViewController(withIdentifier: "DashboardViewController")
                self.navigationController?.pushViewController(DashboardVC, animated: true)
            }
        }
        }

    
    @IBAction func SignInButtonTouchUpInsideAction(_sender: UIButton){
//        let DashboardVC = Utils.getStoryBoard().instantiateViewController(withIdentifier: "DashboardViewController")
//        self.navigationController?.pushViewController(DashboardVC, animated: true)
        FIRAuth.auth()?.signIn(withEmail: EmailTextField.text!, password: PasswordTextField.text!){ (user, error) in
            
                        if (error != nil) {
                          print(error)
                      } else {
                          let DashboardVC = Utils.getStoryBoard().instantiateViewController(withIdentifier: "DashboardViewController")
                          self.navigationController?.pushViewController(DashboardVC, animated: true)
                       }
                  }
        
    }
    
    @IBAction func TouchIdButtonTouchUpInsideAction(_sender: UIButton) {
        let myContext = LAContext()
        let myLocalizedReasonString = "Login with your Touch ID"
        
        var authError: NSError? = nil
        if #available(iOS 8.0, OSX 10.12, *) {
            if myContext.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
                myContext.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: myLocalizedReasonString) { (success, evaluateError) in
                    if (success) {
                        DispatchQueue.main.async {
                        let DashboardVC = Utils.getStoryBoard().instantiateViewController(withIdentifier: "DashboardViewController")
                        self.navigationController?.pushViewController(DashboardVC, animated: true)
                        }
                    } else {
                        // User did not authenticate successfully, look at error and take appropriate action
                    }
                }
            } else {
                // Could not evaluate policy; look at authError and present an appropriate message to user
            }
        } else {
            // Fallback on earlier versions
        }
               
    }

    

}
