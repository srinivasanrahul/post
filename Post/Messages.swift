//
//  Messages.swift
//  Post
//
//  Created by SoftSuave on 23/06/17.
//  Copyright © 2017 Ramesh Vayavuru. All rights reserved.
//

import UIKit

class Messages: NSObject {
    var messageType  : String?
    var messageValue : String?
    var timeStamp    : String?
    var SenderID     : String?    
}
