//
//  ProfileViewController.swift
//  Post
//
//  Created by Ramesh Vayavuru on 03/12/16.
//  Copyright © 2016 Ramesh Vayavuru. All rights reserved.
//

import UIKit
import FirebaseAuth
class ProfileViewController: UIViewController {

    @IBOutlet var ChatSettingsButton: UIButton!
    @IBOutlet weak var LogoutButton: UIButton!
    @IBOutlet weak var ProfileImage: UIImageView!
    @IBOutlet weak var ProfileImageBackground: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        ProfileImage.layer.cornerRadius = ProfileImage.frame.height/2
        ProfileImage.clipsToBounds = true
        ProfileImageBackground.layer.cornerRadius = ProfileImageBackground.frame.height/2
        ProfileImageBackground.clipsToBounds = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func LogoutButtonAction(_sender: UIButton){
        let firebaseAuth = FIRAuth.auth()
        do {
            try firebaseAuth?.signOut()
//             let HomeVC = Utils.getStoryBoard().instantiateViewController(withIdentifier: "HomeViewController")
//             self.navigationController?.pushViewController(HomeVC, animated: true)
            _ = self.navigationController?.popToRootViewController(animated: true)
       } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }

}
