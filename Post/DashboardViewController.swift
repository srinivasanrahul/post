//
//  DashboardViewController.swift
//  Post
//
//  Created by Ramesh Vayavuru on 02/12/16.
//  Copyright © 2016 Ramesh Vayavuru. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var ProfileButton: UIBarButtonItem!
    let images = [#imageLiteral(resourceName: "download_1"), #imageLiteral(resourceName: "download_2"), #imageLiteral(resourceName: "download_3")]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.hidesBackButton = true
        let profileButton = UIBarButtonItem(title: "Profile", style: UIBarButtonItemStyle.plain, target: self, action: #selector(DashboardViewController.ProfileButtonAction(_sender:)))
         self.navigationItem.leftBarButtonItem = profileButton
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func ProfileButtonAction(_sender: UIButton){
    let ProfileVC = Utils.getStoryBoard().instantiateViewController(withIdentifier: "ProfileViewController")
    self.navigationController?.pushViewController(ProfileVC, animated: true)
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection Section:Int)->Int{
    return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath:IndexPath)-> UITableViewCell{
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatTableViewCell", for: indexPath) as! ChatTableViewCell
        let newString: String = String(indexPath.row)
        cell.ProfileDisplayName.text  = "Hello world" + newString
        cell.ProfileImage.image = images[1]
        cell.ProfileImage.layer.cornerRadius = cell.ProfileImage.frame.width/2
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let ChatVC = Utils.getStoryBoard().instantiateViewController(withIdentifier: "ChatViewController")
        self.navigationController?.pushViewController(ChatVC, animated: true)
    }
}
