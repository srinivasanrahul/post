//
//  Utils.swift
//  
//
//  Created by Ramesh Vayavuru on 28/11/16.
//
//

import UIKit

class Utils: NSObject {
   public let firebase_URL = "https://post-f75bd.firebaseio.com/"
    
    class func getStoryBoard() -> UIStoryboard {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard
    }
    
}
